///<reference path="../typings/tsd.d.ts"/>
((): void => {
    'use strict';

    angular
        .module('fenixtest.core', [
            /* Angular Modules  */

            /* 3rd Party Modules */
            'fenix'
        ]);

})();
