///<reference path="../../typings/tsd.d.ts"/>
///<reference path="../../client/_references.ts"/>
module FenixTest.Users{
    'use strict';
    
    export interface IUserController{
        
    }
    
    class UserController implements IUserController {
        static $inject = ['fenix.query.UserQueryService', 'fenix.authentication.AuthenticationService', 'fenix.execution.ExecutionService'];
         constructor(private userService:Fenix.Query.IUserQueryService,
         private authService: Fenix.Authentication.IAuthenticationService,
         private exectionService: Fenix.Execution.IExecutionService){
             var query = userService.createUserQuery();
             var emailAddressFilter = new jo.FilterClause('EmailAddress').eq('andrew@trekkingforcharity.org')
             query.filter(emailAddressFilter)
             userService.runQuery<Fenix.Entities.Queries.IApiQueryResponse<Fenix.Entities.Queries.User.IUser>>(query)
             .then(
                 (data) => {
                     
                 },
                 (reason) => {
                     
                 }                                  
             )
            // this.exectionService.excuteCommand<Fenix.Entities.Commands.User.CreateUserCommand, Fenix.Entities.CommandResults.
            //  var query = userService.createUsersQuery();
            //  var result = userService.runQuery(query)
             debugger;
         }
    }
    
    angular.module('fenixtest.users')
        .controller('fenixtest.users.UserController',
        UserController);
}