///<reference path="../../typings/tsd.d.ts"/>
((): void => {
    'use strict';

    angular
        .module('fenixtest.users', []);
})();