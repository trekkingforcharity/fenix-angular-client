///<reference path="../typings/tsd.d.ts"/>
///<reference path="../client/_references.ts"/>
((): void => {
    'use strict';
    angular
        .module('fenixtest', [
            'fenixtest.core',
            'fenixtest.users'
        ]);
})();