/// <reference path="typings/main.d.ts" />
/// <reference path="external-typings/external-typings.d.ts" />
declare module Fenix.Types {
    class Guid {
        private id;
        private static emptyGuid;
        constructor(id: string);
        static empty(): Guid;
        static newGuid(): Guid;
        static regex(format?: string): RegExp;
        private static s4();
        toString(format: string): string;
        valueOf(): string;
    }
}
declare module Fenix.Types {
    interface ApiError extends Error {
    }
}
declare module Fenix.Entities.CommandResults {
    interface IBaseCommandResult {
    }
}
declare module Fenix.Entities.CommandResults {
    interface IExecutionResult<TBaseCommandResult extends IBaseCommandResult> {
        result: TBaseCommandResult;
        success: boolean;
        failMessage: string;
        errors: any[];
    }
}
declare module Fenix.Entities.CommandResults.Waypoint {
    interface IAddWaypointToTrekCommandResult extends IBaseCommandResult {
    }
    interface IOrderWaypointsCommandResult extends IBaseCommandResult {
    }
    interface IRemoveWaypointFromTrekCommandResult extends IBaseCommandResult {
    }
}
declare module Fenix.Entities.CommandResults.User {
    interface IChangeBasicDetailsCommandResult extends IBaseCommandResult {
    }
    interface IChangeEmailCommandResult extends IBaseCommandResult {
    }
    interface IChangePasswordCommandResult extends IBaseCommandResult {
    }
    interface ICreateUserCommandResult extends IBaseCommandResult {
        userId: Fenix.Types.Guid;
    }
    interface IResetPasswordCommandResult extends IBaseCommandResult {
    }
    interface ISendPasswordResetTokenCommandResult extends IBaseCommandResult {
        token: string;
    }
}
declare module Fenix.Entities.CommandResults.Update {
    interface IAddUpdateToTrekCommandResult extends IBaseCommandResult {
        updateId: Fenix.Types.Guid;
    }
}
declare module Fenix.Entities.CommandResults.Trek {
    interface IActivateTrekCommandResult extends IBaseCommandResult {
    }
    interface IChangeBasicDetailsCommandResult extends IBaseCommandResult {
    }
    interface IChangeTrekNameCommandResult extends IBaseCommandResult {
    }
    interface ICreateTrekCommandResult extends IBaseCommandResult {
        trekId: Fenix.Types.Guid;
    }
    interface IDeactivateTrekCommandResult extends IBaseCommandResult {
    }
}
declare module Fenix.Entities.CommandResults.Auth {
    interface ICreateAuthApplicationCommandResult extends IBaseCommandResult {
        apiKey: Fenix.Types.Guid;
    }
    interface ILoginUserCommandResult extends IBaseCommandResult {
        userId: Fenix.Types.Guid;
        authToken: string;
        expireDate: moment.Moment;
        emailAddress: string;
        firstName: string;
        lastName: string;
    }
    interface IRenewTokenCommandResult extends IBaseCommandResult {
        userId: Fenix.Types.Guid;
        authToken: string;
        expireDate: moment.Moment;
    }
    interface IVerifyUserCommandResult extends IBaseCommandResult {
    }
}
declare module Fenix.Entities.Commands {
    abstract class BaseCommand {
        private route;
        constructor(route: string);
        getRoute(): string;
    }
}
declare module Fenix.Entities.Commands.Waypoint {
    import BaseCommand = Fenix.Entities.Commands.BaseCommand;
    class AddWaypointToTrekCommand extends BaseCommand {
        constructor();
        position: number;
        lng: number;
        lat: number;
        trekId: Fenix.Types.Guid;
        name: string;
    }
    class OrderWaypointsCommand extends BaseCommand {
        constructor();
        trekId: Fenix.Types.Guid;
        newOrder: any;
    }
    class RemoveWaypointFromTrekCommand extends BaseCommand {
        constructor();
        trekId: Fenix.Types.Guid;
        waypointId: Fenix.Types.Guid;
    }
}
declare module Fenix.Entities.Commands.User {
    import BaseCommand = Fenix.Entities.Commands.BaseCommand;
    class ChangeBasicDetailsCommand extends BaseCommand {
        constructor();
        firstName: string;
        lastName: string;
    }
    class ChangeEmailCommand extends BaseCommand {
        constructor();
        oldEmailAddress: string;
        newEmailAddress: string;
    }
    class ChangePasswordCommand extends BaseCommand {
        constructor();
        oldPassword: string;
        newPassword: string;
    }
    class CreateUserCommand extends BaseCommand {
        constructor();
        firstName: string;
        lastName: string;
        emailAddress: string;
        password: string;
    }
    class ResetPasswordCommand extends BaseCommand {
        constructor();
        resetToken: string;
        password: string;
    }
    class SendPasswordResetTokenCommand extends BaseCommand {
        constructor();
        emailAddress: string;
    }
}
declare module Fenix.Entities.Commands.Update {
    import BaseCommand = Fenix.Entities.Commands.BaseCommand;
    class AddUpdateToTrekCommand extends BaseCommand {
        constructor();
        trekId: Fenix.Types.Guid;
        name: string;
        description: string;
        lng: number;
        lat: number;
        coverPhotoFileName: string;
        coverPhotoContentCompressed: string;
    }
}
declare module Fenix.Entities.Commands.Trek {
    import BaseCommand = Fenix.Entities.Commands.BaseCommand;
    class ActivateTrekCommand extends BaseCommand {
        constructor();
        trekId: Fenix.Types.Guid;
    }
    class ChangeBasicDetailsCommand extends BaseCommand {
        constructor();
        description: string;
        trekId: Fenix.Types.Guid;
    }
    class ChangeTrekNameCommand extends BaseCommand {
        constructor();
        trekId: Fenix.Types.Guid;
        name: string;
    }
    class CreateTrekCommand extends BaseCommand {
        constructor();
        name: string;
        description: string;
        coverPhotoFileName: string;
        coverPhotoContentCompressed: string;
    }
    class DeactivateTrekCommand extends BaseCommand {
        constructor();
        trekId: Fenix.Types.Guid;
    }
}
declare module Fenix.Entities.Commands.Auth {
    import BaseCommand = Fenix.Entities.Commands.BaseCommand;
    class CreateAuthApplicationCommand extends BaseCommand {
        constructor();
        userId: Fenix.Types.Guid;
        name: string;
    }
    class LoginUserCommand extends BaseCommand {
        constructor();
        emailAddress: string;
        password: string;
    }
    class RenewTokenCommand extends BaseCommand {
        constructor();
        token: string;
    }
    class VerifyUserCommand extends BaseCommand {
        constructor();
        confirmationToken: string;
    }
}
declare module Fenix.Entities.Queries {
    interface IBaseQuery {
        id: Fenix.Types.Guid;
    }
}
declare module Fenix.Entities.Queries {
    interface IApiQueryResponse<TBaseQuery extends IBaseQuery> {
        hasResults: boolean;
        moreResultsAvailable: boolean;
        page: number;
        results: TBaseQuery[];
        resultsPerPage: number;
        totalPages: number;
        totalResults: number;
    }
}
declare module Fenix.Entities.Queries.Waypoint {
    interface IUser extends IBaseQuery {
        firstName: string;
        lastName: string;
        emailAddress: string;
        isConfirmed: boolean;
        id: Fenix.Types.Guid;
    }
    interface IWaypoint extends IBaseQuery {
        name: string;
        order: number;
        lat: number;
        lng: number;
        trekId: Fenix.Types.Guid;
        id: Fenix.Types.Guid;
    }
}
declare module Fenix.Entities.Queries.User {
    interface IUser extends IBaseQuery {
        firstName: string;
        lastName: string;
        emailAddress: string;
        isConfirmed: boolean;
        id: Fenix.Types.Guid;
    }
}
declare module Fenix.Entities.Queries.Update {
    interface IUpdate extends IBaseQuery {
        name: string;
        description: string;
        position: any;
        trekId: Fenix.Types.Guid;
        trekName: string;
        trekSlug: string;
        createdByUserId: Fenix.Types.Guid;
        firstName: string;
        lastName: string;
        emailAddress: string;
        imageRef: string;
        id: Fenix.Types.Guid;
    }
}
declare module Fenix.Entities.Queries.Trek {
    interface IArchivedTrek extends IBaseQuery {
        name: string;
        slug: string;
        description: string;
        startDate: moment.Moment;
        createdByUserId: Fenix.Types.Guid;
        whenCreated: moment.Moment;
        updateCount: number;
        waypointCount: number;
        imageRef: string;
        isActive: boolean;
        isPublished: boolean;
        id: Fenix.Types.Guid;
    }
    interface ITrek extends IBaseQuery {
        name: string;
        slug: string;
        description: string;
        startDate: moment.Moment;
        createdByUserId: Fenix.Types.Guid;
        whenCreated: moment.Moment;
        updateCount: number;
        waypointCount: number;
        imageRef: string;
        isActive: boolean;
        isPublished: boolean;
        id: Fenix.Types.Guid;
    }
}
declare module Fenix.Entities.Queries.Auth {
    interface IAuthApplication extends IBaseQuery {
        userId: Fenix.Types.Guid;
        name: string;
        whenApplied: moment.Moment;
        apiKey: Fenix.Types.Guid;
        hash: string;
        isActive: boolean;
        id: Fenix.Types.Guid;
    }
    interface IAuthToken extends IBaseQuery {
        token: string;
        whenExpires: moment.Moment;
        whenCreated: moment.Moment;
        userId: Fenix.Types.Guid;
        firstName: string;
        lastName: string;
        emailAddress: string;
        authApplicationId: Fenix.Types.Guid;
        apiKey: Fenix.Types.Guid;
        id: Fenix.Types.Guid;
    }
}
declare module Fenix.Providers {
    interface IRemoteConfig {
        getToken(): string;
        getRootPath(): string;
        getErrorFunction(): Function;
        setToken(value: string): any;
        setRootPath(value: string): any;
        setErrorFunction(value: Function): void;
    }
    class RemoteConfigProvider implements angular.IServiceProvider {
        private _token;
        private _rootPath;
        private _errorResponse;
        setToken(value: string): void;
        setRootPath(value: string): void;
        setErrorFunction(value: Function): void;
        $get(): IRemoteConfig;
    }
}
declare module Fenix.Factories {
    function ErrorInterceptorFactory(remoteConfigProvider: Fenix.Providers.IRemoteConfig, $q: angular.IQService): any;
}
declare module Fenix.Authentication {
    interface IAuthenticationService {
        generateAuthKey(): string;
        token: string;
        expiryDate?: moment.Moment;
        userId: Fenix.Types.Guid;
        newTokenNeeded(): boolean;
        getNewToken(outFunc: () => any): any;
        loginUser(emailAddress: string, password: string): angular.IPromise<boolean>;
    }
}
declare module Fenix.Execution {
    import BaseCommand = Fenix.Entities.Commands.BaseCommand;
    import IBaseCommandResult = Fenix.Entities.CommandResults.IBaseCommandResult;
    import IExecutionResult = Fenix.Entities.CommandResults.IExecutionResult;
    interface IExecutionService {
        excuteCommand<TBaseCommand extends BaseCommand, TBaseCommandResult extends IBaseCommandResult>(cmd: TBaseCommand): angular.IPromise<IExecutionResult<TBaseCommandResult>>;
    }
}
declare module Fenix.Query {
    import IBaseQuery = Fenix.Entities.Queries.IBaseQuery;
    import IApiQueryResponse = Fenix.Entities.Queries.IApiQueryResponse;
    interface IBaseQueryService {
        runQuery<TReadEntity extends IBaseQuery>(query: jo): angular.IPromise<IApiQueryResponse<TReadEntity>>;
    }
    abstract class BaseQueryService implements IBaseQueryService {
        protected $http: angular.IHttpService;
        protected remoteConfig: Fenix.Providers.IRemoteConfig;
        protected authenticationService: Fenix.Authentication.IAuthenticationService;
        constructor($http: angular.IHttpService, remoteConfig: Fenix.Providers.IRemoteConfig, authenticationService: Fenix.Authentication.IAuthenticationService);
        protected generateReadUrl(namespace: string, entity: string): string;
        runQuery<TReadEntity extends IBaseQuery>(query: jo): angular.IPromise<IApiQueryResponse<TReadEntity>>;
        private query<TReadEntity>(query);
    }
}
declare module Fenix.Query {
    interface IWaypointQueryService extends Fenix.Query.IBaseQueryService {
        createUserQuery(): jo;
        createWaypointQuery(): jo;
    }
    interface IUserQueryService extends Fenix.Query.IBaseQueryService {
        createUserQuery(): jo;
    }
    interface IUpdateQueryService extends Fenix.Query.IBaseQueryService {
        createUpdateQuery(): jo;
    }
    interface ITrekQueryService extends Fenix.Query.IBaseQueryService {
        createArchivedTrekQuery(): jo;
        createTrekQuery(): jo;
    }
    interface IAuthQueryService extends Fenix.Query.IBaseQueryService {
        createAuthApplicationQuery(): jo;
        createAuthTokenQuery(): jo;
    }
}
