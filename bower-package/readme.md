﻿# Fenix angular client #
> Angularjs module to connect to Trekking for Charity's 'Fenix' Api.  Built against Fenix 1.0.1.21
## Getting Started ##

This component is hosted on a public feed at [myget.org](https://www.myget.org/feed/trekking-for-charity-bower/package/bower/t4c-fenix)  and so the following needs to be added to the `registry` section of the `.bowerrc` file.

	"registry": {
		"search": [
			"https://www.myget.org/F/trekking-for-charity-bower/bower/",
			"https://bower.herokuapp.com"
		]
	}

Get the package:

	$ bower install t4c-fenix --save-dev

Add the script tags:

	<script src="bower_components/angular/angular.js"></script>
    <script src="bower_components/jodata/jodata.js"></script>
    <script src="bower_components/angular-utf8-base64/angular-utf8-base64.js"></script>
    <script src="bower_components/angular-local-storage/dist/angular-local-storage.js"></script>
	<script src="bower_components/t4c-fenix/fenix-angular-client.js"></script>

Then add the module to your app:

	angular.module('myApp', ['fenix']);


## Sending data ##
The Fenix Api expects data in the form on commands.  These are small objects which contains the only the information that is required and are sent from a central point in the client.

### Execution Service ###
The execution service, found at `Fenix.Execution.IExecutionService`, is used by injecting `'fenix.execution.ExecutionService'` into an angular directive and exposes the following method

	 excuteCommand<TBaseCommand extends BaseCommand, TBaseCommandResult extends IBaseCommandResult>(cmd : TBaseCommand): 
            angular.IPromise<IExecutionResult<TBaseCommandResult>>;

*Usage example*

	angular.module('myApp', ['fenix'])
		.controller('myCtlr', ['fenix.execution.ExecutionService', function(executionService) {
			var cmd = new Fenix.Entities.Commands.User.CreateUserCommand();
			cmd.firstName = 'Andrew';
			cmd.lastName = 'Davis';
			cmd.emailAddress = 'andrew@trekkingforcharity.org';
			cmd.password = 'password';
			exectionService.excuteCommand(cmd)
				.then(function (data) {
                	// Do something with the data 
				},
				 function (reason) {
					// Do something with the error
			});                
		}]);

## Querying data ##
The Fenix Api uses OData filtering to query data, so the client utilizes [joData](https://github.com/mccow002/joData) to generate the query before posting the request.

Like with execution there services for these queries on the client and each service exposes the right endpoints for the area in the Api.  Below details how each endpoint is set up in the client.

 - `fenix.query.WaypointQueryService` (`Fenix.Query.IWaypointQueryService`)
	- `createUserQuery()`
	- `createWaypointQuery()`
 - `fenix.query.UserQueryService` (`Fenix.Query.IUserQueryService`)
	- `createUserQuery()`
 - `fenix.query.UpdateQueryService` (`Fenix.Query.IUpdateQueryService`)
	- `createUpdateQuery()`
 - `fenix.query.TrekQueryService` (`Fenix.Query.ITrekQueryService`)
	- `createArchivedTrekQuery()`
	- `createTrekQuery()`
 - `fenix.query.AuthQueryService` (`Fenix.Query.IAuthQueryService`)
	- `createAuthApplicationQuery()`
	- `createAuthTokenQuery()`

As detailed each service has one or more `createAbcQuery()`.  These methods returns a `joData` object with the server endpoint already added.  Each service also has an additional method `runQuery` where the `joData` is passed into and the query is then executed on the server.

*Usage example*
	
	angular.module('myApp', ['fenix'])
		.controller('myCtlr', ['fenix.query.UserQueryService', function(userQueryService) {
			var query = userQueryService.createUserQuery();
			var emailAddressFilter = new jo.FilterClause('EmailAddress').eq('andrew@trekkingforcharity.org');
			query.filter(emailAddressFilter);
			userQueryService.runQuery(query)
				.then(function (data) {
                	// Do something with the data 
				},
				 function (reason) {
					// Do something with the error
			});
		}]);

## Authentication ##

Fenix's authentication works based on a custom token implementation and this has been implemented deep in this component.  The authentication service `fenix.authentication.AuthenticationService` (`Fenix.Authentication.IAuthenticationService`) has a method to authenticate a user against Fenix, `loginUser`.

## Exception Handling ##

The client injects its own `HttpInterceptor` to detect errors in the server response, and to do something with the error a function can be passed in via the **provider**.

## Provider ##

To set certain parameters and functionality inside the client, a `Provider` is used. Currently it has three methods

### `setApiKey` ###
This sets the Api key needed to communicate with Fenix. 

### `setRootPath` ###
This sets the Url where Fenix is located.

### `setErrorFunction` ###
The function that is used for http error handling