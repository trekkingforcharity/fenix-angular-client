declare module ab {
	interface IBase64 {
		encode(input: string): string;
		decode(input: string): string;
		urlencode(input: string): string;
		urldecode(input: string): string;
	}
}