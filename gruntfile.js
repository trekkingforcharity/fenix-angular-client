require("any-promise/register")("bluebird");
module.exports = function(grunt) {
    grunt.initConfig({
        bower: {
            default: {
                base: 'bower_components', /* the path to the bower_components directory */
                dest: 'wwwroot/lib',
                options: {
                    checkExistence: true,
                    debugging: true,
                    paths: {
                        bowerDirectory: 'bower_components',
                        bowerrc: '.bowerrc',
                        bowerJson: 'bower.json'
                    }
                }
            },
        },
        ts: {
            client: {
                watch: '.',
                files: [
                    { 
                        src: ["client/**/*.ts"], 
                        dest: "wwwroot/client" 
                    }
                ],
                options: {
                    inlineSources: true,
                    inlineSourceMap: true
                }
            },
            app: {
                watch: '.',
                files: [
                    { 
                        src: ["./app/**/*.ts"], 
                        dest: "wwwroot/app" 
                    }
                ],
                options: {
                    inlineSources: true,
                    inlineSourceMap: true
                }
            },
            package: {
                src: ["client/**/*.ts"],
                out: "fenix-angular-client.js",
                options: {
                    declaration: true,
                    sourceMap: false                    
                }
            }
            
        },
        connect: {
            server: {
                options: {
                    port: 9001,
                    base: 'wwwroot',
                    keepalive: true
                }
            }
        },
        concurrent: {
            options: {
                logConcurrentOutput: true
            },
            default: {
                tasks: [
                    "ts:client",
                    "ts:app",
                    "connect"
                ]
            }
        },       
        "bower-install-simple": {
            options: {
                color: true,
                directory: "bower_components"
            },
            "prod": {
                options: {
                    production: true
                }
            },
            "dev": {
                options: {
                    production: false
                }
            }
        },
        tslint: {
            options: {
                configuration: grunt.file.readJSON("./release.tslint.json")
            },
            release : {
                files: {
                    src: [
                        'client/**/*.ts'
                    ]
                }
            }
        },
        bump: {
            options: {
                files: ['bower.json'],
                commitFiles: ['bower.json', 'fenix-angular-client.js', 'fenix-angular-client.d.ts'],
                commitMessage: 'Release v%VERSION% [ci skip]',
                push: false,
                tagName: 'v%VERSION%',
                tagMessage: 'Version %VERSION%'
                 
            }
        },
        typings: {
            install: {}
         }
    });

    grunt.loadNpmTasks('main-bower-files');
    grunt.loadNpmTasks('grunt-ts');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks("grunt-bower-install-simple");
    grunt.loadNpmTasks('grunt-typings');
    grunt.loadNpmTasks("grunt-tslint");
    grunt.loadNpmTasks('grunt-bump');

    grunt.registerTask('default', ['bower:default', 'concurrent:default']); //, 'ts:client', 'ts:app','connect' 
    grunt.registerTask('copy-dependencies', ['bower:dev'])
    grunt.registerTask('generate-client', ['ts:client'])
    grunt.registerTask('start-server', ['connect'])
    grunt.registerTask('release', ['bower-install-simple','typings', 'tslint:release', 'ts:package', 'bump'])
}