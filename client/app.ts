///<reference path="../typings/main.d.ts"/>
///<reference path="../external-typings/external-typings.d.ts"/>
((): void => {
    'use strict';
    angular
        .module('fenix', [
            'fenix.core',
            'fenix.providers',
            'fenix.authentication',
            'fenix.query',
            'fenix.execution',
            'fenix.factories'
        ]);
})();