module Fenix.Authentication {
    import IExecutionResult = Fenix.Entities.CommandResults.IExecutionResult;

    export interface IAuthenticationService {
       generateAuthKey(): string;
       token: string;
       expiryDate? : moment.Moment;
       userId: Fenix.Types.Guid;
       newTokenNeeded() : boolean;
       getNewToken(outFunc : () => any) : any;
       loginUser(emailAddress: string, password: string): angular.IPromise<boolean>;
   }

   class AuthenticationService implements IAuthenticationService {

       static $inject = ['fenix.providers.RemoteConfigProvider', '$http', 'base64', 'localStorageService'];

       token: string;
       expiryDate : moment.Moment;
       userId: Fenix.Types.Guid;

       constructor(private remoteConfigProvider: Fenix.Providers.IRemoteConfig,
        private $http: angular.IHttpService,
        private base64: ab.IBase64,
        private localStorageService: angular.local.storage.ILocalStorageService) {
           this.token = <string>localStorageService.get('token');
           this.expiryDate = <moment.Moment>localStorageService.get('expiryDate');
           this.userId = <Fenix.Types.Guid>localStorageService.get('userId');
       }

       generateAuthKey(): string {
           var authKey : string;
            if (this.expiryDate && this.expiryDate.utc > moment.utc) {
                authKey = this.base64.encode(JSON.stringify( { apiKey : this.remoteConfigProvider.getToken(), token : this.token}));
            } else {
                authKey = this.base64.encode(JSON.stringify( { apiKey : this.remoteConfigProvider.getToken(), token : ''}));
            }
           return authKey;
       }

       newTokenNeeded(): boolean {
            if (!this.token) {
                return false;
            }
            if (this.expiryDate && this.expiryDate.utc > moment.utc) {
                return false;
            }
            return true;
       }

       getNewToken(outFunc : () => any) : any {
           var cmd: Fenix.Entities.Commands.Auth.RenewTokenCommand = new Fenix.Entities.Commands.Auth.RenewTokenCommand();
           cmd.token = this.token;

           return this.$http.post(this.generateCommandUrl(cmd), cmd)
            .then(
                function(promiseValue: angular.IHttpPromiseCallbackArg<any>): any{
                    return outFunc();
                },
                function (reason:any) {
                    console.log(reason);
                }
            );
       }

       loginUser(emailAddress: string, password: string): angular.IPromise<boolean> {
           var cmd = new Fenix.Entities.Commands.Auth.LoginUserCommand();
           cmd.emailAddress = emailAddress;
           cmd.password = password;
           var that = this;
           return this.$http.post(this.generateCommandUrl(cmd), cmd, {
               headers: {
                   'Authorization' : 'Token ' + this.generateAuthKey()
               }
           })
            .then(
                function(promiseValue:
                    angular.IHttpPromiseCallbackArg<IExecutionResult<Fenix.Entities.CommandResults.Auth.ILoginUserCommandResult>>): boolean{
                    if (!promiseValue.data.success) {
                        return false;
                    }

                    that.processLoginResult(promiseValue.data.result);
                    return true;
                },
                function (reason:any) {
                    throw that.handleError(reason);
                }
            );
       }

       private generateCommandUrl(command: Fenix.Entities.Commands.BaseCommand):string {
            return this.remoteConfigProvider.getRootPath()
            + Fenix.Types.Guid.newGuid() + '/execute/'
            + command.getRoute();
        }

       private handleError(originalResponse: any): Fenix.Types.ApiError {
            switch (originalResponse.StatusCode) {
                case 0:
                    return {
                        name : 'ServerNotAvaliable',
                        message : 'Unable to connect to server: ' + this.remoteConfigProvider.getRootPath()
                    };
                default:
                    return {
                        name : 'ErrorNotTyped',
                        message : ''
                    };
            }
        }

        private processRenewTokenResult(data: Fenix.Entities.CommandResults.Auth.IRenewTokenCommandResult):void {
           this.token = data.authToken;
           this.expiryDate = data.expireDate;
           this.userId = data.userId;

           this.localStorageService.set('token', this.token);
           this.localStorageService.set('expiryDate', this.expiryDate);
           this.localStorageService.set('userId', this.userId);
        }

        private processLoginResult(data:Fenix.Entities.CommandResults.Auth.ILoginUserCommandResult):void {
           this.token = data.authToken;
           this.expiryDate = data.expireDate;
           this.userId = data.userId;

           this.localStorageService.set('token', this.token);
           this.localStorageService.set('expiryDate', this.expiryDate);
           this.localStorageService.set('userId', this.userId);
        }
    }

   angular.module('fenix.authentication')
        .service('fenix.authentication.AuthenticationService', AuthenticationService);
}