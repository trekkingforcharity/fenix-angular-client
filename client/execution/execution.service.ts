module Fenix.Execution {
    import BaseCommand = Fenix.Entities.Commands.BaseCommand;
    import IBaseCommandResult = Fenix.Entities.CommandResults.IBaseCommandResult;
    import IExecutionResult = Fenix.Entities.CommandResults.IExecutionResult;

   export interface IExecutionService {
       excuteCommand<TBaseCommand extends BaseCommand, TBaseCommandResult extends IBaseCommandResult>(cmd : TBaseCommand):
            angular.IPromise<IExecutionResult<TBaseCommandResult>>;
   }

   class ExecutionService implements IExecutionService {

       static $inject = ['fenix.providers.RemoteConfigProvider', '$http', 'fenix.authentication.AuthenticationService'];

       constructor(private remoteConfigProvider: Fenix.Providers.IRemoteConfig,
            private $http: angular.IHttpService,
            private authService : Fenix.Authentication.IAuthenticationService) {
       }

       excuteCommand<TBaseCommand extends BaseCommand, TBaseCommandResult extends IBaseCommandResult>(cmd : TBaseCommand):
            angular.IPromise<IExecutionResult<TBaseCommandResult>> {

            if (this.authService.newTokenNeeded()) {
                return this.authService.getNewToken(() => this.execute<TBaseCommand, TBaseCommandResult>(cmd));
            } else {
                return this.execute<TBaseCommand, TBaseCommandResult>(cmd);
            }
       }

       private execute<TBaseCommand extends BaseCommand, TBaseCommandResult extends IBaseCommandResult>(cmd : TBaseCommand):
        angular.IPromise<IExecutionResult<TBaseCommandResult>> {
           var that = this;
           return this.$http.post(this.generateCommandUrl(cmd), cmd, {
               headers: {
                   'Authorization' : 'Token ' + that.authService.generateAuthKey()
               }
           })
            .then(
                function(promiseValue: angular.IHttpPromiseCallbackArg<IExecutionResult<TBaseCommandResult>>):
                    IExecutionResult<TBaseCommandResult>{
                    return promiseValue.data;
                },
                function (reason:any) {
                    throw that.handleError(reason);
                }
            );
       }

       private generateCommandUrl(command: Fenix.Entities.Commands.BaseCommand):string {
            return this.remoteConfigProvider.getRootPath()
            + Fenix.Types.Guid.newGuid() + '/execute/'
            + command.getRoute();
        }

        private handleError(originalResponse: any): Fenix.Types.ApiError {
            switch (originalResponse.StatusCode) {
                case 0:
                    return {
                        name : 'ServerNotAvaliable',
                        message : 'Unable to connect to server: ' + this.remoteConfigProvider.getRootPath()
                    };
                default:
                    return {
                        name : 'ErrorNotTyped',
                        message : ''
                    };
            }
        }
    }

    angular.module('fenix.execution')
        .service('fenix.execution.ExecutionService', ExecutionService);
}