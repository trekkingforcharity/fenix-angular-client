///<reference path="../../typings/main.d.ts"/>
((): void => {
    'use strict';

    angular
        .module('fenix.providers', []);
})();