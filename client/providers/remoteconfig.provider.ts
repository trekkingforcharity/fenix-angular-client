///<reference path="../../typings/main.d.ts"/>
module Fenix.Providers {
    export interface IRemoteConfig {
        getToken(): string;
        getRootPath(): string;
        getErrorFunction(): Function;
        setToken(value: string);
        setRootPath(value: string);
        setErrorFunction(value: Function): void;
    }

    export class RemoteConfigProvider implements angular.IServiceProvider {
        private _token: string;
        private _rootPath: string;
        private  _errorResponse:Function;

        public setToken(value: string) {
            this._token = value;
        }
        public setRootPath(value: string) {
            this._rootPath = value;
        }
        public setErrorFunction (value: Function) {
            this._errorResponse = value;
        }

        $get(): IRemoteConfig {
            return {
                getToken: () => { return this._token; },
                getRootPath: () => { return this._rootPath; },
                getErrorFunction: () => {return this._errorResponse; },
                setToken: (value: string) => { this._token = value; },
                setRootPath: (value: string) => { this._rootPath = value; },
                setErrorFunction: (value: Function) => {this._errorResponse = value; }
            };
        }
    }

    angular.module('fenix.providers')
        .provider('fenix.providers.RemoteConfigProvider', RemoteConfigProvider);
}