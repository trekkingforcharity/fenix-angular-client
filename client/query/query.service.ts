﻿module Fenix.Query {
	export interface IWaypointQueryService extends Fenix.Query.IBaseQueryService {
		createWaypointQuery(): jo;
	}

	class WaypointQueryService extends Fenix.Query.BaseQueryService implements IWaypointQueryService {
		createWaypointQuery(): jo {
			var query: jo = new jo(this.generateReadUrl('Waypoint', 'Waypoint'));
             return query;
		}
	}
	angular.module('fenix.query')
        .service('fenix.query.WaypointQueryService', WaypointQueryService);

	export interface IUserQueryService extends Fenix.Query.IBaseQueryService {
		createUserQuery(): jo;
	}

	class UserQueryService extends Fenix.Query.BaseQueryService implements IUserQueryService {
		createUserQuery(): jo {
			var query: jo = new jo(this.generateReadUrl('User', 'User'));
             return query;
		}
	}
	angular.module('fenix.query')
        .service('fenix.query.UserQueryService', UserQueryService);

	export interface IUpdateQueryService extends Fenix.Query.IBaseQueryService {
		createUpdateQuery(): jo;
	}

	class UpdateQueryService extends Fenix.Query.BaseQueryService implements IUpdateQueryService {
		createUpdateQuery(): jo {
			var query: jo = new jo(this.generateReadUrl('Update', 'Update'));
             return query;
		}
	}
	angular.module('fenix.query')
        .service('fenix.query.UpdateQueryService', UpdateQueryService);

	export interface ITrekQueryService extends Fenix.Query.IBaseQueryService {
		createArchivedTrekQuery(): jo;
		createTrekQuery(): jo;
	}

	class TrekQueryService extends Fenix.Query.BaseQueryService implements ITrekQueryService {
		createArchivedTrekQuery(): jo {
			var query: jo = new jo(this.generateReadUrl('Trek', 'ArchivedTrek'));
             return query;
		}
		createTrekQuery(): jo {
			var query: jo = new jo(this.generateReadUrl('Trek', 'Trek'));
             return query;
		}
	}
	angular.module('fenix.query')
        .service('fenix.query.TrekQueryService', TrekQueryService);

	export interface IAuthQueryService extends Fenix.Query.IBaseQueryService {
		createAuthApplicationQuery(): jo;
		createAuthTokenQuery(): jo;
	}

	class AuthQueryService extends Fenix.Query.BaseQueryService implements IAuthQueryService {
		createAuthApplicationQuery(): jo {
			var query: jo = new jo(this.generateReadUrl('Auth', 'AuthApplication'));
             return query;
		}
		createAuthTokenQuery(): jo {
			var query: jo = new jo(this.generateReadUrl('Auth', 'AuthToken'));
             return query;
		}
	}
	angular.module('fenix.query')
        .service('fenix.query.AuthQueryService', AuthQueryService);

}
