module Fenix.Query {
    import IBaseQuery = Fenix.Entities.Queries.IBaseQuery;
    import IApiQueryResponse = Fenix.Entities.Queries.IApiQueryResponse;

    export interface IBaseQueryService {
        runQuery<TReadEntity extends IBaseQuery>(query: jo) : angular.IPromise<IApiQueryResponse<TReadEntity>>;
    }

    export abstract class BaseQueryService implements IBaseQueryService {

        constructor(protected $http: angular.IHttpService,
            protected remoteConfig: Fenix.Providers.IRemoteConfig,
            protected authenticationService: Fenix.Authentication.IAuthenticationService) {
        }

        protected generateReadUrl(namespace:string, entity: string):string {
            return this.remoteConfig.getRootPath()
            + Fenix.Types.Guid.newGuid() + '/read/' + namespace + '/'
            + entity;
        }

        public runQuery<TReadEntity extends IBaseQuery>(query: jo) : angular.IPromise<IApiQueryResponse<TReadEntity>> {
             if (this.authenticationService.newTokenNeeded()) {
                return this.authenticationService.getNewToken(() => this.query<TReadEntity>(query));
             }

             return this.query<TReadEntity>(query);
         }

         private query<TReadEntity extends IBaseQuery>(query: jo) : angular.IPromise<IApiQueryResponse<TReadEntity>> {
             return this.$http.get(query.toString(), {
                 headers: {
                     'Authorization' : 'Token ' + this.authenticationService.generateAuthKey()
                 }
             }).then(
                 function(promiseValue: angular.IHttpPromiseCallbackArg<IApiQueryResponse<TReadEntity>>): IApiQueryResponse<TReadEntity> {
                     return promiseValue.data;
                 },
                 function (reason:any) {
                      console.log(reason);
                 }
             );
         }
    }
}