((): void => {
    'use strict';

    function config(
        localStorageServiceProvider: angular.local.storage.ILocalStorageServiceProvider,
        $httpProvider: angular.IHttpProvider
        ) {
        localStorageServiceProvider.setPrefix('fenix');
        localStorageServiceProvider.setStorageType('sessionStorage');
        $httpProvider.interceptors.push('fenix.factories.ErrorInterceptorFactory');
    }
    angular
        .module('fenix')
        .config(config);

    config.$inject = ['localStorageServiceProvider', '$httpProvider'];

})();