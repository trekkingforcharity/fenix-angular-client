module Fenix.Factories {

    export function ErrorInterceptorFactory(
        remoteConfigProvider: Fenix.Providers.IRemoteConfig,
        $q: angular.IQService
    ): any {
        return {
            request: function (config) {
                return config || $q.when(config);
            },
            requestError: function(request){
                return $q.reject(request);
            },
            response: function (response) {
                return response || $q.when(response);
            },
            responseError: function (response) {
                if (response) {
                    switch  (response.status) {
                        case 200:
                            break;
                        default:
                            remoteConfigProvider.getErrorFunction()(response);
                            break;
                    }
                }
                console.log(response);
                return $q.reject(response);
            }
        };
    }
    ErrorInterceptorFactory.$inject = ['fenix.providers.RemoteConfigProvider', '$q'];

    angular.module('fenix.factories')
        .factory('fenix.factories.ErrorInterceptorFactory', ErrorInterceptorFactory);
}