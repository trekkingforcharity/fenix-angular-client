﻿module Fenix.Entities.Commands.Waypoint {
	import BaseCommand = Fenix.Entities.Commands.BaseCommand;

	export class AddWaypointToTrekCommand extends BaseCommand {

		constructor() {
			super('Waypoint/AddWaypointToTrekCommand');
		}
		position: number;
		lng: number;
		lat: number;
		trekId: Fenix.Types.Guid;
		name: string;
	}

	export class OrderWaypointsCommand extends BaseCommand {

		constructor() {
			super('Waypoint/OrderWaypointsCommand');
		}
		trekId: Fenix.Types.Guid;
		newOrder: any; //IEnumerable`1;
	}

	export class RemoveWaypointFromTrekCommand extends BaseCommand {

		constructor() {
			super('Waypoint/RemoveWaypointFromTrekCommand');
		}
		trekId: Fenix.Types.Guid;
		waypointId: Fenix.Types.Guid;
	}
}
module Fenix.Entities.Commands.User {
	import BaseCommand = Fenix.Entities.Commands.BaseCommand;

	export class ChangeBasicDetailsCommand extends BaseCommand {

		constructor() {
			super('User/ChangeBasicDetailsCommand');
		}
		firstName: string;
		lastName: string;
	}

	export class ChangeEmailCommand extends BaseCommand {

		constructor() {
			super('User/ChangeEmailCommand');
		}
		oldEmailAddress: string;
		newEmailAddress: string;
	}

	export class ChangePasswordCommand extends BaseCommand {

		constructor() {
			super('User/ChangePasswordCommand');
		}
		oldPassword: string;
		newPassword: string;
	}

	export class CreateUserCommand extends BaseCommand {

		constructor() {
			super('User/CreateUserCommand');
		}
		firstName: string;
		lastName: string;
		emailAddress: string;
		password: string;
	}

	export class ResetPasswordCommand extends BaseCommand {

		constructor() {
			super('User/ResetPasswordCommand');
		}
		resetToken: string;
		password: string;
	}

	export class SendPasswordResetTokenCommand extends BaseCommand {

		constructor() {
			super('User/SendPasswordResetTokenCommand');
		}
		emailAddress: string;
	}
}
module Fenix.Entities.Commands.Update {
	import BaseCommand = Fenix.Entities.Commands.BaseCommand;

	export class AddUpdateToTrekCommand extends BaseCommand {

		constructor() {
			super('Update/AddUpdateToTrekCommand');
		}
		trekId: Fenix.Types.Guid;
		name: string;
		description: string;
		lng: number;
		lat: number;
		coverPhotoFileName: string;
		coverPhotoContentCompressed: string;
	}
}
module Fenix.Entities.Commands.Trek {
	import BaseCommand = Fenix.Entities.Commands.BaseCommand;

	export class ActivateTrekCommand extends BaseCommand {

		constructor() {
			super('Trek/ActivateTrekCommand');
		}
		trekId: Fenix.Types.Guid;
	}

	export class ChangeBasicDetailsCommand extends BaseCommand {

		constructor() {
			super('Trek/ChangeBasicDetailsCommand');
		}
		description: string;
		trekId: Fenix.Types.Guid;
	}

	export class ChangeTrekNameCommand extends BaseCommand {

		constructor() {
			super('Trek/ChangeTrekNameCommand');
		}
		trekId: Fenix.Types.Guid;
		name: string;
	}

	export class CreateTrekCommand extends BaseCommand {

		constructor() {
			super('Trek/CreateTrekCommand');
		}
		name: string;
		description: string;
		coverPhotoFileName: string;
		coverPhotoContentCompressed: string;
	}

	export class DeactivateTrekCommand extends BaseCommand {

		constructor() {
			super('Trek/DeactivateTrekCommand');
		}
		trekId: Fenix.Types.Guid;
	}
}
module Fenix.Entities.Commands.Auth {
	import BaseCommand = Fenix.Entities.Commands.BaseCommand;

	export class CreateAuthApplicationCommand extends BaseCommand {

		constructor() {
			super('Auth/CreateAuthApplicationCommand');
		}
		userId: Fenix.Types.Guid;
		name: string;
	}

	export class LoginUserCommand extends BaseCommand {

		constructor() {
			super('Auth/LoginUserCommand');
		}
		emailAddress: string;
		password: string;
	}

	export class RenewTokenCommand extends BaseCommand {

		constructor() {
			super('Auth/RenewTokenCommand');
		}
		token: string;
	}

	export class VerifyUserCommand extends BaseCommand {

		constructor() {
			super('Auth/VerifyUserCommand');
		}
		confirmationToken: string;
	}
}
