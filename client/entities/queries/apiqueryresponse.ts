module Fenix.Entities.Queries {
    export interface IApiQueryResponse<TBaseQuery extends IBaseQuery> {
        hasResults: boolean;
        moreResultsAvailable: boolean;
        page: number;
        results: TBaseQuery[];
        resultsPerPage: number;
        totalPages: number;
        totalResults: number;
    }
}