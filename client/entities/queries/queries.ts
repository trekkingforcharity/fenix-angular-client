﻿module Fenix.Entities.Queries.Waypoint {
	export interface IWaypoint extends IBaseQuery {
		name: string;
		lat: number;
		lng: number;
		trekId: Fenix.Types.Guid;
		id: Fenix.Types.Guid;
	}
}
module Fenix.Entities.Queries.User {
	export interface IUser extends IBaseQuery {
		firstName: string;
		lastName: string;
		emailAddress: string;
		isConfirmed: boolean;
		id: Fenix.Types.Guid;
	}
}
module Fenix.Entities.Queries.Update {
	export interface IUpdate extends IBaseQuery {
		description: string;
		lng: number;
		lat: number;
		trekId: Fenix.Types.Guid;
		id: Fenix.Types.Guid;
	}
}
module Fenix.Entities.Queries.Trek {
	export interface IArchivedTrek extends IBaseQuery {
		name: string;
		slug: string;
		description: string;
		startDate: moment.Moment;
		createdByUserId: Fenix.Types.Guid;
		whenCreated: moment.Moment;
		updateCount: number;
		waypointCount: number;
		isActive: boolean;
		isPublished: boolean;
		id: Fenix.Types.Guid;
	}
	export interface ITrek extends IBaseQuery {
		name: string;
		slug: string;
		description: string;
		startDate: any; //DateTimeOffset;
		createdByUserId: Fenix.Types.Guid;
		whenCreated: moment.Moment;
		updateCount: number;
		waypointCount: number;
		isActive: boolean;
		isPublished: boolean;
		id: Fenix.Types.Guid;
	}
}
module Fenix.Entities.Queries.Auth {
	export interface IAuthApplication extends IBaseQuery {
		userId: Fenix.Types.Guid;
		name: string;
		whenApplied: moment.Moment;
		apiKey: Fenix.Types.Guid;
		hash: string;
		isActive: boolean;
		id: Fenix.Types.Guid;
	}
	export interface IAuthToken extends IBaseQuery {
		token: string;
		whenExpires: moment.Moment;
		whenCreated: moment.Moment;
		userId: Fenix.Types.Guid;
		firstName: string;
		lastName: string;
		emailAddress: string;
		authApplicationId: Fenix.Types.Guid;
		apiKey: Fenix.Types.Guid;
		id: Fenix.Types.Guid;
	}
}
