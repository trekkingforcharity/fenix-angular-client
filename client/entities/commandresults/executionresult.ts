module Fenix.Entities.CommandResults {
    export interface IExecutionResult<TBaseCommandResult extends IBaseCommandResult> {
        result: TBaseCommandResult;
        success: boolean;
        failMessage: string;
        errors: any[];
    }
}