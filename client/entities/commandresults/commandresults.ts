﻿module Fenix.Entities.CommandResults.Waypoint {
	export interface IAddWaypointToTrekCommandResult extends IBaseCommandResult {
	}

	export interface IOrderWaypointsCommandResult extends IBaseCommandResult {
	}

	export interface IRemoveWaypointFromTrekCommandResult extends IBaseCommandResult {
	}

}
module Fenix.Entities.CommandResults.User {
	export interface IChangeBasicDetailsCommandResult extends IBaseCommandResult {
	}

	export interface IChangeEmailCommandResult extends IBaseCommandResult {
	}

	export interface IChangePasswordCommandResult extends IBaseCommandResult {
	}

	export interface ICreateUserCommandResult extends IBaseCommandResult {
		userId: Fenix.Types.Guid;
	}

	export interface IResetPasswordCommandResult extends IBaseCommandResult {
	}

	export interface ISendPasswordResetTokenCommandResult extends IBaseCommandResult {
		token: string;
	}

}
module Fenix.Entities.CommandResults.Update {
	export interface IAddUpdateToTrekCommandResult extends IBaseCommandResult {
		updateId: Fenix.Types.Guid;
	}

}
module Fenix.Entities.CommandResults.Trek {
	export interface IActivateTrekCommandResult extends IBaseCommandResult {
	}

	export interface IChangeBasicDetailsCommandResult extends IBaseCommandResult {
	}

	export interface IChangeTrekNameCommandResult extends IBaseCommandResult {
	}

	export interface ICreateTrekCommandResult extends IBaseCommandResult {
		trekId: Fenix.Types.Guid;
	}

	export interface IDeactivateTrekCommandResult extends IBaseCommandResult {
	}

}
module Fenix.Entities.CommandResults.Auth {
	export interface ICreateAuthApplicationCommandResult extends IBaseCommandResult {
		apiKey: Fenix.Types.Guid;
	}

	export interface ILoginUserCommandResult extends IBaseCommandResult {
		userId: Fenix.Types.Guid;
		authToken: string;
		expireDate: moment.Moment;
		emailAddress: string;
		firstName: string;
		lastName: string;
	}

	export interface IRenewTokenCommandResult extends IBaseCommandResult {
		userId: Fenix.Types.Guid;
		authToken: string;
		expireDate: moment.Moment;
	}

	export interface IVerifyUserCommandResult extends IBaseCommandResult {
	}

}
