///<reference path="../typings/main.d.ts"/>
((): void => {
    'use strict';

    angular
        .module('fenix.core', [
            /* Angular Modules  */

            /* 3rd Party Modules */
            'ab-base64',
            'LocalStorageModule'
        ]);

})();
