var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Fenix;
(function (Fenix) {
    var Types;
    (function (Types) {
        var Guid = (function () {
            function Guid(id) {
                this.id = id.toLowerCase();
            }
            Guid.empty = function () {
                return Guid.emptyGuid;
            };
            Guid.newGuid = function () {
                return new Guid(Guid.s4() + Guid.s4() + '-' + Guid.s4() + '-' + Guid.s4() + '-' +
                    Guid.s4() + '-' + Guid.s4() + Guid.s4() + Guid.s4());
            };
            Guid.regex = function (format) {
                switch (format) {
                    case 'x':
                    case 'X':
                        return (/\{[a-z0-9]{8}(?:-[a-z0-9]{4}){3}-[a-z0-9]{12}\}/i);
                    default:
                        return (/[a-z0-9]{8}(?:-[a-z0-9]{4}){3}-[a-z0-9]{12}/i);
                }
            };
            Guid.s4 = function () {
                return Math
                    .floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            };
            Guid.prototype.toString = function (format) {
                switch (format) {
                    case 'x':
                    case 'X':
                        return '{' + this.id + '}';
                    default:
                        return this.id;
                }
            };
            Guid.prototype.valueOf = function () {
                return this.id;
            };
            Guid.emptyGuid = new Guid('00000000-0000-0000-0000-000000000000');
            return Guid;
        }());
        Types.Guid = Guid;
    })(Types = Fenix.Types || (Fenix.Types = {}));
})(Fenix || (Fenix = {}));
var Fenix;
(function (Fenix) {
    var Entities;
    (function (Entities) {
        var Commands;
        (function (Commands) {
            var BaseCommand = (function () {
                function BaseCommand(route) {
                    this.route = route;
                }
                BaseCommand.prototype.getRoute = function () {
                    return this.route;
                };
                return BaseCommand;
            }());
            Commands.BaseCommand = BaseCommand;
        })(Commands = Entities.Commands || (Entities.Commands = {}));
    })(Entities = Fenix.Entities || (Fenix.Entities = {}));
})(Fenix || (Fenix = {}));
var Fenix;
(function (Fenix) {
    var Entities;
    (function (Entities) {
        var Commands;
        (function (Commands) {
            var Waypoint;
            (function (Waypoint) {
                var BaseCommand = Fenix.Entities.Commands.BaseCommand;
                var AddWaypointToTrekCommand = (function (_super) {
                    __extends(AddWaypointToTrekCommand, _super);
                    function AddWaypointToTrekCommand() {
                        _super.call(this, 'Waypoint/AddWaypointToTrekCommand');
                    }
                    return AddWaypointToTrekCommand;
                }(BaseCommand));
                Waypoint.AddWaypointToTrekCommand = AddWaypointToTrekCommand;
                var OrderWaypointsCommand = (function (_super) {
                    __extends(OrderWaypointsCommand, _super);
                    function OrderWaypointsCommand() {
                        _super.call(this, 'Waypoint/OrderWaypointsCommand');
                    }
                    return OrderWaypointsCommand;
                }(BaseCommand));
                Waypoint.OrderWaypointsCommand = OrderWaypointsCommand;
                var RemoveWaypointFromTrekCommand = (function (_super) {
                    __extends(RemoveWaypointFromTrekCommand, _super);
                    function RemoveWaypointFromTrekCommand() {
                        _super.call(this, 'Waypoint/RemoveWaypointFromTrekCommand');
                    }
                    return RemoveWaypointFromTrekCommand;
                }(BaseCommand));
                Waypoint.RemoveWaypointFromTrekCommand = RemoveWaypointFromTrekCommand;
            })(Waypoint = Commands.Waypoint || (Commands.Waypoint = {}));
        })(Commands = Entities.Commands || (Entities.Commands = {}));
    })(Entities = Fenix.Entities || (Fenix.Entities = {}));
})(Fenix || (Fenix = {}));
var Fenix;
(function (Fenix) {
    var Entities;
    (function (Entities) {
        var Commands;
        (function (Commands) {
            var User;
            (function (User) {
                var BaseCommand = Fenix.Entities.Commands.BaseCommand;
                var ChangeBasicDetailsCommand = (function (_super) {
                    __extends(ChangeBasicDetailsCommand, _super);
                    function ChangeBasicDetailsCommand() {
                        _super.call(this, 'User/ChangeBasicDetailsCommand');
                    }
                    return ChangeBasicDetailsCommand;
                }(BaseCommand));
                User.ChangeBasicDetailsCommand = ChangeBasicDetailsCommand;
                var ChangeEmailCommand = (function (_super) {
                    __extends(ChangeEmailCommand, _super);
                    function ChangeEmailCommand() {
                        _super.call(this, 'User/ChangeEmailCommand');
                    }
                    return ChangeEmailCommand;
                }(BaseCommand));
                User.ChangeEmailCommand = ChangeEmailCommand;
                var ChangePasswordCommand = (function (_super) {
                    __extends(ChangePasswordCommand, _super);
                    function ChangePasswordCommand() {
                        _super.call(this, 'User/ChangePasswordCommand');
                    }
                    return ChangePasswordCommand;
                }(BaseCommand));
                User.ChangePasswordCommand = ChangePasswordCommand;
                var CreateUserCommand = (function (_super) {
                    __extends(CreateUserCommand, _super);
                    function CreateUserCommand() {
                        _super.call(this, 'User/CreateUserCommand');
                    }
                    return CreateUserCommand;
                }(BaseCommand));
                User.CreateUserCommand = CreateUserCommand;
                var ResetPasswordCommand = (function (_super) {
                    __extends(ResetPasswordCommand, _super);
                    function ResetPasswordCommand() {
                        _super.call(this, 'User/ResetPasswordCommand');
                    }
                    return ResetPasswordCommand;
                }(BaseCommand));
                User.ResetPasswordCommand = ResetPasswordCommand;
                var SendPasswordResetTokenCommand = (function (_super) {
                    __extends(SendPasswordResetTokenCommand, _super);
                    function SendPasswordResetTokenCommand() {
                        _super.call(this, 'User/SendPasswordResetTokenCommand');
                    }
                    return SendPasswordResetTokenCommand;
                }(BaseCommand));
                User.SendPasswordResetTokenCommand = SendPasswordResetTokenCommand;
            })(User = Commands.User || (Commands.User = {}));
        })(Commands = Entities.Commands || (Entities.Commands = {}));
    })(Entities = Fenix.Entities || (Fenix.Entities = {}));
})(Fenix || (Fenix = {}));
var Fenix;
(function (Fenix) {
    var Entities;
    (function (Entities) {
        var Commands;
        (function (Commands) {
            var Update;
            (function (Update) {
                var BaseCommand = Fenix.Entities.Commands.BaseCommand;
                var AddUpdateToTrekCommand = (function (_super) {
                    __extends(AddUpdateToTrekCommand, _super);
                    function AddUpdateToTrekCommand() {
                        _super.call(this, 'Update/AddUpdateToTrekCommand');
                    }
                    return AddUpdateToTrekCommand;
                }(BaseCommand));
                Update.AddUpdateToTrekCommand = AddUpdateToTrekCommand;
            })(Update = Commands.Update || (Commands.Update = {}));
        })(Commands = Entities.Commands || (Entities.Commands = {}));
    })(Entities = Fenix.Entities || (Fenix.Entities = {}));
})(Fenix || (Fenix = {}));
var Fenix;
(function (Fenix) {
    var Entities;
    (function (Entities) {
        var Commands;
        (function (Commands) {
            var Trek;
            (function (Trek) {
                var BaseCommand = Fenix.Entities.Commands.BaseCommand;
                var ActivateTrekCommand = (function (_super) {
                    __extends(ActivateTrekCommand, _super);
                    function ActivateTrekCommand() {
                        _super.call(this, 'Trek/ActivateTrekCommand');
                    }
                    return ActivateTrekCommand;
                }(BaseCommand));
                Trek.ActivateTrekCommand = ActivateTrekCommand;
                var ChangeBasicDetailsCommand = (function (_super) {
                    __extends(ChangeBasicDetailsCommand, _super);
                    function ChangeBasicDetailsCommand() {
                        _super.call(this, 'Trek/ChangeBasicDetailsCommand');
                    }
                    return ChangeBasicDetailsCommand;
                }(BaseCommand));
                Trek.ChangeBasicDetailsCommand = ChangeBasicDetailsCommand;
                var ChangeTrekNameCommand = (function (_super) {
                    __extends(ChangeTrekNameCommand, _super);
                    function ChangeTrekNameCommand() {
                        _super.call(this, 'Trek/ChangeTrekNameCommand');
                    }
                    return ChangeTrekNameCommand;
                }(BaseCommand));
                Trek.ChangeTrekNameCommand = ChangeTrekNameCommand;
                var CreateTrekCommand = (function (_super) {
                    __extends(CreateTrekCommand, _super);
                    function CreateTrekCommand() {
                        _super.call(this, 'Trek/CreateTrekCommand');
                    }
                    return CreateTrekCommand;
                }(BaseCommand));
                Trek.CreateTrekCommand = CreateTrekCommand;
                var DeactivateTrekCommand = (function (_super) {
                    __extends(DeactivateTrekCommand, _super);
                    function DeactivateTrekCommand() {
                        _super.call(this, 'Trek/DeactivateTrekCommand');
                    }
                    return DeactivateTrekCommand;
                }(BaseCommand));
                Trek.DeactivateTrekCommand = DeactivateTrekCommand;
            })(Trek = Commands.Trek || (Commands.Trek = {}));
        })(Commands = Entities.Commands || (Entities.Commands = {}));
    })(Entities = Fenix.Entities || (Fenix.Entities = {}));
})(Fenix || (Fenix = {}));
var Fenix;
(function (Fenix) {
    var Entities;
    (function (Entities) {
        var Commands;
        (function (Commands) {
            var Auth;
            (function (Auth) {
                var BaseCommand = Fenix.Entities.Commands.BaseCommand;
                var CreateAuthApplicationCommand = (function (_super) {
                    __extends(CreateAuthApplicationCommand, _super);
                    function CreateAuthApplicationCommand() {
                        _super.call(this, 'Auth/CreateAuthApplicationCommand');
                    }
                    return CreateAuthApplicationCommand;
                }(BaseCommand));
                Auth.CreateAuthApplicationCommand = CreateAuthApplicationCommand;
                var LoginUserCommand = (function (_super) {
                    __extends(LoginUserCommand, _super);
                    function LoginUserCommand() {
                        _super.call(this, 'Auth/LoginUserCommand');
                    }
                    return LoginUserCommand;
                }(BaseCommand));
                Auth.LoginUserCommand = LoginUserCommand;
                var RenewTokenCommand = (function (_super) {
                    __extends(RenewTokenCommand, _super);
                    function RenewTokenCommand() {
                        _super.call(this, 'Auth/RenewTokenCommand');
                    }
                    return RenewTokenCommand;
                }(BaseCommand));
                Auth.RenewTokenCommand = RenewTokenCommand;
                var VerifyUserCommand = (function (_super) {
                    __extends(VerifyUserCommand, _super);
                    function VerifyUserCommand() {
                        _super.call(this, 'Auth/VerifyUserCommand');
                    }
                    return VerifyUserCommand;
                }(BaseCommand));
                Auth.VerifyUserCommand = VerifyUserCommand;
            })(Auth = Commands.Auth || (Commands.Auth = {}));
        })(Commands = Entities.Commands || (Entities.Commands = {}));
    })(Entities = Fenix.Entities || (Fenix.Entities = {}));
})(Fenix || (Fenix = {}));
(function () {
    'use strict';
    angular
        .module('fenix.providers', []);
})();
var Fenix;
(function (Fenix) {
    var Providers;
    (function (Providers) {
        var RemoteConfigProvider = (function () {
            function RemoteConfigProvider() {
            }
            RemoteConfigProvider.prototype.setToken = function (value) {
                this._token = value;
            };
            RemoteConfigProvider.prototype.setRootPath = function (value) {
                this._rootPath = value;
            };
            RemoteConfigProvider.prototype.setErrorFunction = function (value) {
                this._errorResponse = value;
            };
            RemoteConfigProvider.prototype.$get = function () {
                var _this = this;
                return {
                    getToken: function () { return _this._token; },
                    getRootPath: function () { return _this._rootPath; },
                    getErrorFunction: function () { return _this._errorResponse; },
                    setToken: function (value) { _this._token = value; },
                    setRootPath: function (value) { _this._rootPath = value; },
                    setErrorFunction: function (value) { _this._errorResponse = value; }
                };
            };
            return RemoteConfigProvider;
        }());
        Providers.RemoteConfigProvider = RemoteConfigProvider;
        angular.module('fenix.providers')
            .provider('fenix.providers.RemoteConfigProvider', RemoteConfigProvider);
    })(Providers = Fenix.Providers || (Fenix.Providers = {}));
})(Fenix || (Fenix = {}));
(function () {
    'use strict';
    angular
        .module('fenix.factories', []);
})();
var Fenix;
(function (Fenix) {
    var Factories;
    (function (Factories) {
        function ErrorInterceptorFactory(remoteConfigProvider, $q) {
            return {
                request: function (config) {
                    return config || $q.when(config);
                },
                requestError: function (request) {
                    return $q.reject(request);
                },
                response: function (response) {
                    return response || $q.when(response);
                },
                responseError: function (response) {
                    if (response) {
                        switch (response.status) {
                            case 200:
                                break;
                            default:
                                remoteConfigProvider.getErrorFunction()(response);
                                break;
                        }
                    }
                    console.log(response);
                    return $q.reject(response);
                }
            };
        }
        Factories.ErrorInterceptorFactory = ErrorInterceptorFactory;
        ErrorInterceptorFactory.$inject = ['fenix.providers.RemoteConfigProvider', '$q'];
        angular.module('fenix.factories')
            .factory('fenix.factories.ErrorInterceptorFactory', ErrorInterceptorFactory);
    })(Factories = Fenix.Factories || (Fenix.Factories = {}));
})(Fenix || (Fenix = {}));
(function () {
    'use strict';
    angular
        .module('fenix.authentication', []);
})();
var Fenix;
(function (Fenix) {
    var Authentication;
    (function (Authentication) {
        var AuthenticationService = (function () {
            function AuthenticationService(remoteConfigProvider, $http, base64, localStorageService) {
                this.remoteConfigProvider = remoteConfigProvider;
                this.$http = $http;
                this.base64 = base64;
                this.localStorageService = localStorageService;
                this.token = localStorageService.get('token');
                this.expiryDate = localStorageService.get('expiryDate');
                this.userId = localStorageService.get('userId');
            }
            AuthenticationService.prototype.generateAuthKey = function () {
                var authKey;
                if (this.expiryDate && this.expiryDate.utc > moment.utc) {
                    authKey = this.base64.encode(JSON.stringify({ apiKey: this.remoteConfigProvider.getToken(), token: this.token }));
                }
                else {
                    authKey = this.base64.encode(JSON.stringify({ apiKey: this.remoteConfigProvider.getToken(), token: '' }));
                }
                return authKey;
            };
            AuthenticationService.prototype.newTokenNeeded = function () {
                if (!this.token) {
                    return false;
                }
                if (this.expiryDate && this.expiryDate.utc > moment.utc) {
                    return false;
                }
                return true;
            };
            AuthenticationService.prototype.getNewToken = function (outFunc) {
                var cmd = new Fenix.Entities.Commands.Auth.RenewTokenCommand();
                cmd.token = this.token;
                return this.$http.post(this.generateCommandUrl(cmd), cmd)
                    .then(function (promiseValue) {
                    return outFunc();
                }, function (reason) {
                    console.log(reason);
                });
            };
            AuthenticationService.prototype.loginUser = function (emailAddress, password) {
                var cmd = new Fenix.Entities.Commands.Auth.LoginUserCommand();
                cmd.emailAddress = emailAddress;
                cmd.password = password;
                var that = this;
                return this.$http.post(this.generateCommandUrl(cmd), cmd, {
                    headers: {
                        'Authorization': 'Token ' + this.generateAuthKey()
                    }
                })
                    .then(function (promiseValue) {
                    if (!promiseValue.data.success) {
                        return false;
                    }
                    that.processLoginResult(promiseValue.data.result);
                    return true;
                }, function (reason) {
                    throw that.handleError(reason);
                });
            };
            AuthenticationService.prototype.generateCommandUrl = function (command) {
                return this.remoteConfigProvider.getRootPath()
                    + Fenix.Types.Guid.newGuid() + '/execute/'
                    + command.getRoute();
            };
            AuthenticationService.prototype.handleError = function (originalResponse) {
                switch (originalResponse.StatusCode) {
                    case 0:
                        return {
                            name: 'ServerNotAvaliable',
                            message: 'Unable to connect to server: ' + this.remoteConfigProvider.getRootPath()
                        };
                    default:
                        return {
                            name: 'ErrorNotTyped',
                            message: ''
                        };
                }
            };
            AuthenticationService.prototype.processRenewTokenResult = function (data) {
                this.token = data.authToken;
                this.expiryDate = data.expireDate;
                this.userId = data.userId;
                this.localStorageService.set('token', this.token);
                this.localStorageService.set('expiryDate', this.expiryDate);
                this.localStorageService.set('userId', this.userId);
            };
            AuthenticationService.prototype.processLoginResult = function (data) {
                this.token = data.authToken;
                this.expiryDate = data.expireDate;
                this.userId = data.userId;
                this.localStorageService.set('token', this.token);
                this.localStorageService.set('expiryDate', this.expiryDate);
                this.localStorageService.set('userId', this.userId);
            };
            AuthenticationService.$inject = ['fenix.providers.RemoteConfigProvider', '$http', 'base64', 'localStorageService'];
            return AuthenticationService;
        }());
        angular.module('fenix.authentication')
            .service('fenix.authentication.AuthenticationService', AuthenticationService);
    })(Authentication = Fenix.Authentication || (Fenix.Authentication = {}));
})(Fenix || (Fenix = {}));
(function () {
    'use strict';
    angular
        .module('fenix.execution', []);
})();
var Fenix;
(function (Fenix) {
    var Execution;
    (function (Execution) {
        var ExecutionService = (function () {
            function ExecutionService(remoteConfigProvider, $http, authService) {
                this.remoteConfigProvider = remoteConfigProvider;
                this.$http = $http;
                this.authService = authService;
            }
            ExecutionService.prototype.excuteCommand = function (cmd) {
                var _this = this;
                if (this.authService.newTokenNeeded()) {
                    return this.authService.getNewToken(function () { return _this.execute(cmd); });
                }
                else {
                    return this.execute(cmd);
                }
            };
            ExecutionService.prototype.execute = function (cmd) {
                var that = this;
                return this.$http.post(this.generateCommandUrl(cmd), cmd, {
                    headers: {
                        'Authorization': 'Token ' + that.authService.generateAuthKey()
                    }
                })
                    .then(function (promiseValue) {
                    return promiseValue.data;
                }, function (reason) {
                    throw that.handleError(reason);
                });
            };
            ExecutionService.prototype.generateCommandUrl = function (command) {
                return this.remoteConfigProvider.getRootPath()
                    + Fenix.Types.Guid.newGuid() + '/execute/'
                    + command.getRoute();
            };
            ExecutionService.prototype.handleError = function (originalResponse) {
                switch (originalResponse.StatusCode) {
                    case 0:
                        return {
                            name: 'ServerNotAvaliable',
                            message: 'Unable to connect to server: ' + this.remoteConfigProvider.getRootPath()
                        };
                    default:
                        return {
                            name: 'ErrorNotTyped',
                            message: ''
                        };
                }
            };
            ExecutionService.$inject = ['fenix.providers.RemoteConfigProvider', '$http', 'fenix.authentication.AuthenticationService'];
            return ExecutionService;
        }());
        angular.module('fenix.execution')
            .service('fenix.execution.ExecutionService', ExecutionService);
    })(Execution = Fenix.Execution || (Fenix.Execution = {}));
})(Fenix || (Fenix = {}));
(function () {
    'use strict';
    angular
        .module('fenix.query', []);
})();
var Fenix;
(function (Fenix) {
    var Query;
    (function (Query) {
        var BaseQueryService = (function () {
            function BaseQueryService($http, remoteConfig, authenticationService) {
                this.$http = $http;
                this.remoteConfig = remoteConfig;
                this.authenticationService = authenticationService;
            }
            BaseQueryService.prototype.generateReadUrl = function (namespace, entity) {
                return this.remoteConfig.getRootPath()
                    + Fenix.Types.Guid.newGuid() + '/read/' + namespace + '/'
                    + entity;
            };
            BaseQueryService.prototype.runQuery = function (query) {
                var _this = this;
                if (this.authenticationService.newTokenNeeded()) {
                    return this.authenticationService.getNewToken(function () { return _this.query(query); });
                }
                return this.query(query);
            };
            BaseQueryService.prototype.query = function (query) {
                return this.$http.get(query.toString(), {
                    headers: {
                        'Authorization': 'Token ' + this.authenticationService.generateAuthKey()
                    }
                }).then(function (promiseValue) {
                    return promiseValue.data;
                }, function (reason) {
                    console.log(reason);
                });
            };
            return BaseQueryService;
        }());
        Query.BaseQueryService = BaseQueryService;
    })(Query = Fenix.Query || (Fenix.Query = {}));
})(Fenix || (Fenix = {}));
var Fenix;
(function (Fenix) {
    var Query;
    (function (Query) {
        var WaypointQueryService = (function (_super) {
            __extends(WaypointQueryService, _super);
            function WaypointQueryService() {
                _super.apply(this, arguments);
            }
            WaypointQueryService.prototype.createUserQuery = function () {
                var query = new jo(this.generateReadUrl('Waypoint', 'User'));
                return query;
            };
            WaypointQueryService.prototype.createWaypointQuery = function () {
                var query = new jo(this.generateReadUrl('Waypoint', 'Waypoint'));
                return query;
            };
            return WaypointQueryService;
        }(Fenix.Query.BaseQueryService));
        angular.module('fenix.query')
            .service('fenix.query.WaypointQueryService', WaypointQueryService);
        var UserQueryService = (function (_super) {
            __extends(UserQueryService, _super);
            function UserQueryService() {
                _super.apply(this, arguments);
            }
            UserQueryService.prototype.createUserQuery = function () {
                var query = new jo(this.generateReadUrl('User', 'User'));
                return query;
            };
            return UserQueryService;
        }(Fenix.Query.BaseQueryService));
        angular.module('fenix.query')
            .service('fenix.query.UserQueryService', UserQueryService);
        var UpdateQueryService = (function (_super) {
            __extends(UpdateQueryService, _super);
            function UpdateQueryService() {
                _super.apply(this, arguments);
            }
            UpdateQueryService.prototype.createUpdateQuery = function () {
                var query = new jo(this.generateReadUrl('Update', 'Update'));
                return query;
            };
            return UpdateQueryService;
        }(Fenix.Query.BaseQueryService));
        angular.module('fenix.query')
            .service('fenix.query.UpdateQueryService', UpdateQueryService);
        var TrekQueryService = (function (_super) {
            __extends(TrekQueryService, _super);
            function TrekQueryService() {
                _super.apply(this, arguments);
            }
            TrekQueryService.prototype.createArchivedTrekQuery = function () {
                var query = new jo(this.generateReadUrl('Trek', 'ArchivedTrek'));
                return query;
            };
            TrekQueryService.prototype.createTrekQuery = function () {
                var query = new jo(this.generateReadUrl('Trek', 'Trek'));
                return query;
            };
            return TrekQueryService;
        }(Fenix.Query.BaseQueryService));
        angular.module('fenix.query')
            .service('fenix.query.TrekQueryService', TrekQueryService);
        var AuthQueryService = (function (_super) {
            __extends(AuthQueryService, _super);
            function AuthQueryService() {
                _super.apply(this, arguments);
            }
            AuthQueryService.prototype.createAuthApplicationQuery = function () {
                var query = new jo(this.generateReadUrl('Auth', 'AuthApplication'));
                return query;
            };
            AuthQueryService.prototype.createAuthTokenQuery = function () {
                var query = new jo(this.generateReadUrl('Auth', 'AuthToken'));
                return query;
            };
            return AuthQueryService;
        }(Fenix.Query.BaseQueryService));
        angular.module('fenix.query')
            .service('fenix.query.AuthQueryService', AuthQueryService);
    })(Query = Fenix.Query || (Fenix.Query = {}));
})(Fenix || (Fenix = {}));
(function () {
    'use strict';
    angular
        .module('fenix.core', [
        'ab-base64',
        'LocalStorageModule'
    ]);
})();
(function () {
    'use strict';
    angular
        .module('fenix', [
        'fenix.core',
        'fenix.providers',
        'fenix.authentication',
        'fenix.query',
        'fenix.execution',
        'fenix.factories'
    ]);
})();
(function () {
    'use strict';
    function config(localStorageServiceProvider, $httpProvider) {
        localStorageServiceProvider.setPrefix('fenix');
        localStorageServiceProvider.setStorageType('sessionStorage');
        $httpProvider.interceptors.push('fenix.factories.ErrorInterceptorFactory');
    }
    angular
        .module('fenix')
        .config(config);
    config.$inject = ['localStorageServiceProvider', '$httpProvider'];
})();
